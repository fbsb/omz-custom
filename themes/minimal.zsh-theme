# ZSH Theme - Preview: https://gyazo.com/8becc8a7ed5ab54a0262a470555c3eed.png
local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"

if [[ $UID -eq 0 ]]; then
    local user_symbol='#'
else
    local user_symbol='$'
fi

local user_host
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    local user_host='(ssh) %n@%m '
else
    local user_host=''
fi

local current_dir='%~'
local git_branch='$(git_prompt_info)'
local rvm_ruby='$(ruby_prompt_info)'
local venv_prompt='$(virtualenv_prompt_info)'
local KUBE_PS1='$(kube_ps1)'

KUBE_PS1_SYMBOL_ENABLE="false"
KUBE_PS1_NS_ENABLE="${KUBE_PS1_NS_ENABLE:-true}"
KUBE_PS1_CONTEXT_ENABLE="${KUBE_PS1_CONTEXT_ENABLE:-true}"
KUBE_PS1_PREFIX=" | "
KUBE_PS1_SEPARATOR=" | "
KUBE_PS1_DIVIDER=" | "
KUBE_PS1_SUFFIX=""
KUBE_PS1_CTX_COLOR="white"
KUBE_PS1_NS_COLOR="white"
# KUBE_PS1_BG_COLOR="${KUBE_PS1_BG_COLOR}"

PROMPT="${current_dir}${KUBE_PS1}${git_branch}
${user_host}%B${user_symbol}%b "
RPROMPT="%B${return_code}%b"

ZSH_THEME_GIT_PROMPT_PREFIX=" | "
ZSH_THEME_GIT_PROMPT_SUFFIX=""
