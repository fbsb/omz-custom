SRC_BASENAME=$(dirname $(realpath $0))
SRC_BINPATH="${SRC_BASENAME}/bin"
export PATH="$PATH:$SRC_BINPATH"

_src_cd() {
    d=$(src-find $@)
    [[ -n $d ]] && pushd $d
}

alias src=_src_cd
